============
django-icons
============

Free icons sets bundled for use in django projects
(requires django.contrib.staticfiles or django-staticfiles).

Public Domain, MIT, BSD, WTFPL, LGPL-licensed sets are preferred.

django-icons is splitted into several packages in order to reduce
bandwidth consumption.

Usage
=====

Install package using pip, add app to INSTALLED_APPS and use staticfiles.

Icon sets
=========

tango
-----

:Installation: pip install django-icons-tango
:App name: icons_tango
:Url: {{ STATIC_URL }}/icons/tango/
:Origin: http://tango.freedesktop.org/Tango_Icon_Library
:License: Public Domain

icojoy
-------

:Installation: pip install django-icons-icojoy
:App name: icons_icojoy
:Url: {{ STATIC_URL }}/icons/icojoy/
:Origin: http://www.icojoy.com/
:License:
    These icons are free to use in any kind of commercial or
    non-commercial project unlimited times.

icojoy-pointers
---------------

:Installation: pip install django-icons-icojoy-pointers
:App name: icons_icojoy_pointers
:Url: {{ STATIC_URL }}/icons/icojoy-pointers/
:Origin: http://www.icojoy.com/articles/39/
:License:
    These icons are free to use in any kind of commercial or
    non-commercial project unlimited times.

twitterjoy
----------

:Installation: pip install django-icons-twitterjoy
:App name: icons_twitterjoy
:Url: {{ STATIC_URL }}/icons/twitterjoy/
:Origin: http://www.icojoy.com/
:License:
    These icons are free to use in any kind of commercial or
    non-commercial project unlimited times.

backtopixel
-----------

:Installation: pip install django-icons-backtopixel
:App name: icons_backtopixel
:Url: {{ STATIC_URL }}/icons/backtopixel/
:Origin: http://www.icojoy.com/
:License:
    These icons are free to use in any kind of commercial or
    non-commercial project unlimited times.


onebit
------

:Installation: pip install django-icons-onebit
:App name: icons_icojoy
:Url: {{ STATIC_URL }}/icons/onebit/
:Origin: http://www.icojoy.com/
:License:
    These icons are free to use in any kind of commercial or
    non-commercial project unlimited times.

splashyicons
------------

:Installation: pip install django-icons-splashyicons
:App name: icons_splashyicons
:Url: {{ STATIC_URL }}/icons/splashyicons/
:Origin: http://splashyfish.com/sketches/comments/splashy-icons-an-icon-library-for-prototyping
:License:
    It’s free for whatever you want to do with it and no attribution
    is required. Just don’t be an ass about it or karma is coming your
    way whether you believe it or not (i.e. you shall be transformed into
    a skinny pig in this lifetime, struck by really awesome lightning, etc).

web_control
-----------

:Installation: pip install django-icons-web_control
:App name: icons_web_control
:Url: {{ STATIC_URL }}/icons/web_control/
:Origin: http://marko.isfoundhere.com/old-site/webcontrolicons.php
:License: Public Domain

crystal
-------

Crystal icons bundle is not a part of django-icons but there are
similar packages available:

* https://github.com/LangaCore/django-crystal-small
* https://github.com/LangaCore/django-crystal-big

:Url: {{ STATIC_URL }}/crystal/
:Origin: http://www.everaldo.com/crystal/
:License: LGPL

Contributing
============

Source code:

* https://bitbucket.org/kmike/django-icons/

Bug tracker:

* https://bitbucket.org/kmike/django-icons/issues


License for django-icons
========================

The license for django-icons source code is MIT.
